mod util;

use std::time::{Instant, Duration};
use std::collections::VecDeque;
use structopt::StructOpt;
use crate::util::Tree;
use crate::util::{gen_permutation2, gen_permutation};

fn print_title() {
    println!("==================================================");
    println!("                   Rust program");
    println!("==================================================\n");
}

fn timed_gen_perm(n: usize) -> Vec<usize> {
    println!("Using gen_permutation");
    let t = Instant::now();
    let perm = util::gen_permutation(n);
    let dt = t.elapsed().as_millis();
    println!("Permutation generation time: {}ms\n", dt);
    perm
}

fn timed_gen_perm2(n: usize) -> VecDeque<usize> {
    println!("Using gen_permutation");
    let t = Instant::now();
    let perm = util::gen_permutation2(1, n);
    let dt = t.elapsed().as_millis();
    println!("Permutation generation time: {}ms\n", dt);
    perm
}

fn timed_search(n: usize, tree: &Tree) -> u128 {
    print!("Searching {} ... ", n);
    let t = Instant::now();
    let res = tree.search(n);
    let dt = t.elapsed().as_millis();
    if res { print!("Found ! ") } else  { print!("Not found ! ") };
    println!("Searching time: {}ms", dt);
    if !res { tree.print() }
    dt
}

fn perform_search(tree: &Tree, searchlist: Vec<usize>) -> u128 {
    let mut sum = 0;
    for n in searchlist {
        sum += timed_search(n, tree);
    }
    sum / 10
}

fn timed_compress(tree: Tree) -> Tree {
    print!("Compressing ... ");
    let t = Instant::now();
    let ct = tree.compress();
    let dt = t.elapsed().as_millis();
    println!("Done.");
    println!("Compression time: {}ms\n", dt);
    ct
}

/// Configuration options
#[derive(StructOpt, Debug)]
#[structopt(name = "config")]
struct Opt {
    /// Uses gen_permutation instead of gen_perumation2
    #[structopt(short = "g", long)]
    gen_perm: bool,

    /// Disables execution on uncompressed tree
    #[structopt(short = "N", long)]
    no_uncompressed: bool,

    /// Disables execution on compressed tree
    #[structopt(short = "n", long)]
    no_compressed: bool,

    /// Sets the size of the list to build the tree from
    #[structopt(short, long, default_value = "100")]
    size: usize,
}

fn main() {
    print_title();
    let opt = Opt::from_args();
    let searchlist = (1..11).into_iter().collect::<Vec<usize>>();
    if opt.gen_perm {
        let list = timed_gen_perm(opt.size);
        let t = Tree::from_list(list);
        if !(opt.no_uncompressed) {
            println!("Average searching time in uncompressed tree: {}s\n", perform_search(&t, searchlist.clone()));
        }
        if !(opt.no_compressed) {
            let ct = timed_compress(t);
            println!("Average searching time in compressed tree: {}s\n", perform_search(&ct, searchlist));
        }
    } else {
        let list = timed_gen_perm2(opt.size);
        let t = Tree::from_list(Vec::from(list));
        if !(opt.no_uncompressed) {
            println!("Average searching time in uncompressed tree: {}s\n", perform_search(&t, searchlist.clone()));
        }
        if !(opt.no_compressed) {
            let ct = timed_compress(t);
            println!("Average searching time in compressed tree: {}s\n", perform_search(&ct, searchlist));
        }
    }
    println!("End of program.\n");
}
